
## Description

Этот репозиторий создан для выполнения домашних заданий курса MLOps 3.0 от команды ODS.ai.

Личный проект курса и его репозиторий ведутся с использованием подходов методологии [Data Science Lifecycle Process](https://github.com/dslp/dslp?tab=readme-ov-file).

## Contibuting

Каким образом участникам внести собственные изменения в код проекта описано в CONTRIBUTE.md
