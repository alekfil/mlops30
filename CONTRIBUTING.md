# Особенности работы репозитория

В данном репозитории в качестве линтера и форматера используется [ruff](https://docs.astral.sh/ruff/). Для проверки типов - [mypy](https://www.mypy-lang.org/).

Также в данном репозитории используется [pre-commit](https://pre-commit.com/).

# Contribution

Для внесения собственных наработок участнику необходимо:

1. Клонировать репозиторий
```
git clone git@gitlab.com:alekfil/mlops30
```
2. Перейти в папку с репозиторием
```
cd mlops30
```
3. Создать новую ветку Git dev-%user_name%-%short_description_up_to_10_literal%
```
git switch --create #name_branch#
```
4. Установить зависимости окружения с использованием poetry
```
poetry install
```
5. Активировать pre-commit
```
pre-commit install
```
6. Внести изменения в код
7. Создать коммит
```
git commit -m "Short message"
```
8. Запушить изменения в репозиторий
```
git push --set-upstream origin #name_branch#
```
9.  Создать пулл-реквест в ветку dev
